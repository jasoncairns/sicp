;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.1

(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

(define (make-rat n d)
  (let ((g (gcd n d)))
    (cons (/ n g) (/ d g))))

(define (numer x) (car x))

(define (denom x) (cdr x))

(define (add-rat x y)
  (make-rat (+ (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (sub-rat x y)
  (make-rat (- (* (numer x) (denom y))
               (* (numer y) (denom x)))
            (* (denom x) (denom y))))

(define (mul-rat x y)
  (make-rat (* (numer x) (numer y))
            (* (denom x) (denom y))))

(define (div-rat x y)
  (make-rat (* (numer x) (denom y))
            (* (denom x) (numer y))))

(define (equal-rat? x y)
  (= (* (numer x) (denom y))
     (* (numer y) (denom x))))

(define (print-rat x)
  (newline)
  (display (numer x))
  (display "/")
  (display (denom x)))

(let ((one-third (make-rat 1 3)))
  (print-rat (add-rat one-third one-third)))

;;; question already answered in code given!!

(numer (make-rat 1 1))
(denom (make-rat 1 1))
(numer (make-rat 1 -1))
(denom (make-rat 1 -1))
(numer (make-rat -1 1))
(denom (make-rat -1 1))
(numer (make-rat -1 -1))
(denom (make-rat -1 -1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.2

(define (average a b) (/ (+ a b) 2))

(define (make-point x y) (cons x y))

(define (x-point point) (car point))

(define (y-point point) (cdr point))

(define (make-segment start end) (cons start end))

(define (start-segment segment) (car segment))

(define (end-segment segment) (cdr segment))

(define (midpoint-segment segment)
  (let ((start-x (x-point (start-segment segment)))
	(start-y (y-point (start-segment segment)))
	(end-x (x-point (end-segment segment)))
	(end-y (y-point (end-segment segment))))
    (make-point (average start-x end-x) (average start-y end-y))))

(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

(print-point (midpoint-segment (make-segment (make-point 0 0)
					     (make-point 6 9))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.3

(define (make-rectangle bottom-left-point top-right-point)
  (let ((top-left-point (make-point (x-point bottom-left-point)
				    (y-point top-right-point))))
    (cons (make-segment bottom-left-point top-left-point)
	  (make-segment top-left-point top-right-point))))

(define (vertical-rectangle rectangle)
  (car rectangle))

(define (horizontal-rectangle rectangle)
  (cdr rectangle))

(define (length segment)
  (let ((ab-length (lambda (point)
		     (square (- (point (start-segment segment))
				(point (end-segment segment)))))))
    (sqrt (+ (ab-length x-point)
	     (ab-length y-point)))))

(define (perimeter rectangle)
  (* 2
     (+ (length (vertical-rectangle rectangle))
	(length (horizontal-rectangle rectangle)))))

(define (area rectangle)
     (* (length (vertical-rectangle rectangle))
	(length (horizontal-rectangle rectangle))))

(let ((x (make-rectangle (make-point 0 0) (make-point 2 2))))
  (perimeter x)
  (newline)
  (area x))


;;; second representation

(define (make-rectangle vertical-segment horizontal-segment)
  (let ((vertical-start (start-segment vertical-segment))
	(vertical-end (end-segment vertical-segment))
	(horizontal-start (start-segment horizontal-segment))
	(horizontal-end (end-segment horizontal-segment)))
    (let ((bottom-vertical-y (y-point (if (< (y-point vertical-start)
					     (y-point vertical-end))
				      vertical-start
				      vertical-end)))
	  (horizontal-right-x (x-point (if (< (x-point horizontal-start)
					      (x-point horizontal-end))
					   horizontal-end
					   horizontal-start))))
      (cons (make-point (x-point vertical-start) bottom-vertical-y)
	    (make-point horizontal-right-x (y-point horizontal-start))))))

(define (horizontal-rectangle rectangle)
  (make-segment (car rectangle)
		(make-point (x-point (car rectangle)) (y-point (cdr rectangle)))))

(define (vertical-rectangle rectangle)
  (make-segment (make-point (x-point (car rectangle)) (y-point (cdr rectangle)))
		(cdr rectangle)))

(let ((x (make-rectangle (make-segment (make-point 0 0) (make-point 0 2))
			 (make-segment (make-point 0 2) (make-point 2 2)))))
  (display (perimeter x))
  (newline)
  (display (area x)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.4

(define (cons x y)
  (lambda (m) (m x y)))

(define (car z)
  (z (lambda (p q) p)))

(car (cons 1 2))

(car (cons 1 2))
((cons 1 2) (lambda (p q) p))
((lambda (m) (m 1 2)) (lambda (p q) p))
((lambda (p q) p) 1 2)
1

(define (cdr z)
  (z (lambda (p q) q)))

(cdr (cons 1 2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;2.5
;; 2^n = 2*2^(n-1)
;;     = 2k (even)
;;
;; 3^n = 3*3^(n-1)
;;     = (2+1)*3^(n-1)
;;     = 2*3^(n-1) + 1*3^(n-1)
;;     = 2*3^(n-1) + 2*3^(n-2) + 1*3^(n-2)
;;     = 2(3^(n-1) + 3^(n-2) + ... + 3^(n-n)) + 1*3(n-n)
;;     = 2k + 1 (odd)
;;
;; 2^n * 3^n = 2k * (2k+1)
;;           = 4k + 2k
;;	     = 6k
;;           = 2*3k
;;	     = 2j (even)
;;
;; Continually divide by 2 until odd; the number of iterations required gives
;; $a$. Conversely, division by 3 until the number is no longer a multiple of
;; 3 gives $b$
